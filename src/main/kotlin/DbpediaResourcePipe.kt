/*
 * dbpedia resource transformations
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.logging.log4j.Logger
import org.metafacture.framework.objects.Triple
import org.metafacture.io.ObjectJavaIoWriter
import org.metafacture.mangling.DuplicateObjectFilter
import org.metafacture.metamorph.Metamorph
import org.metafacture.monitoring.ObjectLogger
import org.metafacture.strings.StringReader
import org.metafacture.triples.StreamToTriples
import org.metafacture.triples.TripleCollect
import org.swissbib.linked.linkeddata.ESBulkEncoder
import org.swissbib.linked.linkeddata.NtriplesDecoder
import java.io.*
import kotlin.system.exitProcess

class DbpediaResourcePipe(fileName: String, private val log: Logger) {
    private val writerFactory = WriterFactory()
    private val pipe = init(fileName)

    fun process(value: String): String {
        pipe.process(value)
        pipe.closeStream()
        val output = writerFactory.writer.toString()
        pipe.resetStream()
        return output.trim()
    }

    private fun init(fileName: String): StringReader {
        val reader = StringReader()
        reader
                .setReceiver(nTriplesDecoder())
                .setReceiver(StreamToTriples())
                .setReceiver(DuplicateObjectFilter())
                .setReceiver(TripleCollect())
                .setReceiver(Metamorph(loadMorphFile(fileName)))
                .setReceiver(bulkEncoder())
                .setReceiver(ObjectLogger())
                .setReceiver(ObjectJavaIoWriter(writerFactory))
        return reader
    }

    private fun nTriplesDecoder(): NtriplesDecoder {
        val decoder = NtriplesDecoder()
        decoder.setUnicodeEscapeSeq("true")
        return decoder
    }


    private fun bulkEncoder(): ESBulkEncoder {
        val esBulkEncoder = ESBulkEncoder()
        esBulkEncoder.setEscapeChars("true")
        esBulkEncoder.setHeader("false")
        esBulkEncoder.setIndex("")
        return esBulkEncoder
    }

    private fun loadMorphFile(fileName: String): InputStream {
        val file = File("/configs/$fileName.xml")
        return if (file.isFile)
            FileInputStream(file)
        else {
            try {
                ClassLoader.getSystemResourceAsStream("$fileName.xml")
            } catch (ex: IOException) {
                log.error(ex.message)
                exitProcess(1)
            }
        }
    }
}