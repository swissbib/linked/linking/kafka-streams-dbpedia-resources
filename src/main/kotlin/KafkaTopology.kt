/*
 * wikidata-entities transformer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern

class KafkaTopology(private val properties: Properties, private val log: Logger) {
    private val to = properties.getProperty("kafka.topic.sink")
    private val organisationsPattern = Pattern.compile("<http://www\\.w3\\.org/1999/02/22-rdf-syntax-ns#type> <(http://dbpedia\\.org/ontology/Organisation|http://schema\\.org/Organization)>")
    private val personsPattern = Pattern.compile("<http://www\\.w3\\.org/1999/02/22-rdf-syntax-ns#type> <(http://dbpedia\\.org/ontology/Person|http://www\\.wikidata\\.org/entity/Q5)>")
    private val eventsPattern = Pattern.compile("<http://www\\.w3\\.org/1999/02/22-rdf-syntax-ns#type> <(http://dbpedia\\.org/ontology/Event|http://schema\\.org/Event)>")
    private val pattern = Pattern.compile("\"@id\":\"(.*?)\"")

    private val personsPipe = DbpediaResourcePipe("personsMorph", log)
    private val organisationsPipe = DbpediaResourcePipe("organisationsMorph", log)
    private val eventsPipe = DbpediaResourcePipe("eventsMorph", log)
    private val othersPipe = DbpediaResourcePipe("othersMorph", log)

    private fun createSet(filename: String): Set<String> {
        val set = mutableSetOf<String>()
        ClassLoader.getSystemResourceAsStream("$filename.csv")?.bufferedReader(Charset.defaultCharset())?.lines()?.forEach {
            set.add(it.substringAfterLast('/'))
        }
        if (set.isEmpty()) {
            log.error("Could not load instance classes from $filename. The set is empty.")
        }
        return set
    }


    fun build(): Topology {

        val builder = StreamsBuilder()

        val branches = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .mapValues { value -> value }
            .branch(
                Predicate { _, value -> personsBranch(value.data) },
                Predicate { _, value -> branchOrganisation(value.data) },
                Predicate { _, value -> branchEvents(value.data) },
                Predicate { _, _ -> true }
            )

        branches[0]
            .map { _, value -> pipe(value, personsPipe, "elastic.index.persons") }
            .to(to)

        branches[1]
            .map { _, value -> pipe(value, organisationsPipe, "elastic.index.organisations") }
            .to(to)

        branches[2]
            .map { _, value -> pipe(value, eventsPipe, "elastic.index.events") }
            .to(to)

        branches[3]
            .map { _, value -> pipe(value, othersPipe, "elastic.index.others") }
            .to(to)

        return builder.build()
    }


    private fun personsBranch(data: String): Boolean {
        return personsPattern.matcher(data).find()
    }

    private fun branchOrganisation(data: String): Boolean {
        return organisationsPattern.matcher(data).find()
    }

    private fun branchEvents(data: String): Boolean {
        return eventsPattern.matcher(data).find()
    }

    private fun pipe(value: SbMetadataModel, pipe: DbpediaResourcePipe, index: String): KeyValue<String, SbMetadataModel> {
        val result = pipe.process(value.data)
        val matcher = pattern.matcher(result)
        matcher.find()
        return KeyValue(matcher.group(1),
            createSbMetadateModel(result, "${properties.getProperty(index)}-${value.messageDate}"))
    }

    private fun createSbMetadateModel(data: String, index: String): SbMetadataModel {
        return SbMetadataModel()
            .setData(data)
            .setEsIndexName(index)
            .setEsBulkAction(EsBulkActions.INDEX)
    }
}
