# dbpedia-triples-to-json

Kafka Streams application consuming [DBPedia](https://dbpedia.org) data as 
[N-Triples](https://www.w3.org/TR/n-triples/) and converting statements
to [JSON-LD](https://json-ld.org/). Intended to be used with 
[kafka-producer-files](https://gitlab.com/swissbib/linked/kafka-producer-files)
as producer and 
[elastic-bulk-consumer](http://gitlab.com/swissbib/linked/elastic-bulk-consumer)
as consumer.

## Usage

It is recommended that you use the Docker images which can be found on
the 
[repository registry](https://gitlab.com/swissbib/linked/dbpedia-triples-to-json/container_registry).
If you want to override the [settings](src/main/resources/default.properties)
 (what you normally should), provide a customised `.properties` file and
 mount it to `/config/app.properties`.
 
 ```bash
 docker run -v<path/to/your/config/file>:/data/ dbpedia-triples-to-json
```